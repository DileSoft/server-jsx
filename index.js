/** @jsx createElement */
/*** @jsxFrag createFragment */

const createElement = (tag, props, ...children) => {
    return tag(props, children);
  };
  
const createFragment = (props, children) => {
    return children;
};

const Sum = (props) => {
    return props.a + props.b;
}

const Minus = (props) => {
    return props.a - props.b;
}

const MultiplyChildren = (props, children) => {
    return children[0] * children[1];
}

const Compute = () => (
<MultiplyChildren>
    <Sum a={4} b={2}/>
    <Minus a={100} b={50}/>
</MultiplyChildren>
);

console.log(Compute());